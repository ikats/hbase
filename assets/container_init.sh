#! /bin/bash

set -e

CLUSTER_MODE=${CLUSTER_MODE:-true}

chmod 500 /root/.ssh/config

if "${CLUSTER_MODE}"
then
  export HBASE_MANAGES_ZK="false"
  bash inject_configuration.sh
  echo "Waiting for HBase hosts peers to kick in by hbase operator before starting that HBase instance"
  while [ "$(wc -l /etc/hbase/hosts | head -n 1 | cut -d ' ' -f1)" -eq "0" ]
  do
    echo "Could not discover hosts peers, waiting."
    sleep 5
  done

  cat /etc/hbase/hosts > /etc/hosts;
  [[ ! -z $IS_MASTER ]] && \
    (bash "${HBASE_HOME}/bin/start-hbase.sh" && \
    bash "${HBASE_HOME}/bin/hbase-daemon.sh" start rest \
    || exit 1) && echo "Master started."

  test -d ${HBASE_HOME}/logs/ && tail -f ${HBASE_HOME}/logs/* &
  while true
  do
    cat /etc/hbase/hosts > /etc/hosts;
    sleep 10
  done
else
  export HBASE_MANAGES_ZK="true"
  echo "docker-compose mode"

  sed "s/\${HOSTNAME}/$HOSTNAME/g" /root/hbase-site-template.xml > "${HBASE_HOME}/conf/hbase-site.xml"
  sed "s/\${HOSTNAME}/$HOSTNAME/g" /root/zoo-template.cfg > "${HBASE_HOME}/conf/zoo.cfg"

  "${HBASE_HOME}/bin/hbase" thrift start &
  "${HBASE_HOME}/bin/hbase" rest start &
  "${HBASE_HOME}/bin/hbase" master start &
  sleep infinity
fi